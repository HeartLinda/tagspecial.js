# TagSpecial.js

## v1.0

This tool lets you do special things on your Tumblr blog depending on tags (as well as pages).

It was originally designed as an enhancement to [CurtainBlock.js](http://github.com/owlet57/CurtainBlock.js) to hide the curtain on certain pages that are clearly "safe for work," but you can set it up to do other things.

## Basic Setup

If your Web coding skills aren't that great, you can set it up to hide the curtain for CurtainBlock.js. It's a bit involved, but here it is:

### 1. Set up CurtainBlock.js

If you haven't already, set up [CurtainBlock.js](http://github.com/owlet57/CurtainBlock.js). Instructions are at the link.

### 2. Remove Old Code

Find this line:

	<script>CurtainBlock( ... );</script>

Copy down the options in parentheses somewhere (you'll need them later) and then remove the entire line.

### 3. Add New Code

Add this code in its place:

	<script type="application/javascript" src="//www.googledrive.com/host/0B-dv77MFZ5kNOW1NV2FwXzJqZTQ"></script>
	<script type="application/javascript">
		//<![CDATA[
		var TagSpOptions = {
			tags: ['SFW', 'mod'],
			pages: ['donate', 'ask', 'submit'],
			domain: 'example.tumblr.com',
			persistent: true,
			transparent: false,
			redirect: 'about:blank',

			todo: function(isspecial) {
				if(isspecial) {
					//Hide the curtain
					$('.CurtainBlock').hide();
				} else {
					//Show the curtain
					CurtainBlock(this.persistent, this.transparent, this.redirect);
				}
			}
		}
		TagSpecial(TagSpOptions);
		//]]>
	</script>

### 4. Set Options

The new code is a bit complicated, but the only things you really need to worry about are the options: `tags`, `pages`, and so on.

* `tags` is a list of tags to "whitelist." The curtain will be hidden when browsing these tags or viewing a post with this tag. There are some examples for you to get started. If you don't have any tags to whitelist, take out everything in the brackets, until you're left with `[]`.
* `pages` is a list of custom pages to "whitelist." The curtain will be hidden on these pages. As with tags, if you don't have any pages to whitelist, take out everything in the brackets.
* `domain` is your blog's base URL. Usually this is `[yourname].tumblr.com`, but if you have a custom domain, make sure to put that here.
* The `persistent`, `transparent`, and `redirect` options are borrowed from CurtainBlock.js. Those are the options you copied down in Step 2. For example, if you originally setup CurtainBlock.js like this:

		CurtainBlock(true, false, "http://www.google.com")

	then you should turn that into this:

		persistent: true,
		transparent: false,
		redirect: "http://blog.owlet57.com"

	If you had fewer than three options before, start by changing `persistent` and leave the others as they are in the example.

## Advanced Setup

You don't need CurtainBlock.js for this to work, but you will need [jQuery](http://jquery.com/). (CurtainBlock.js already requires jQuery, which is why this wasn't in the basic setup.)

This script defines the function `TagSpecial()`, which does all the work. It takes an object as its only parameter, which should contain the properties `tags`, `pages`, `domain`, and `todo`. The `tags` and `pages` properties are arrays of strings, and `domain` is a string. The `todo` property is a function that takes a single boolean argument; it will be called with `true` if the Tumblr page matches a tag or custom page, `false` if not.

## License

TagSpecial.js is available under a "two-clause" BSD license, with additional permission for the compressed JavaScript. Please read `LICENSE.txt` for details.