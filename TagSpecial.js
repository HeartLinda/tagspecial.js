function TagSpecial(options) {
	$(function() {

	var locref = location.pathname.toLowerCase().split('/');
	var isspecial = false;
	//Check tags
	$.each(options.tags, function(i, val) {
		//Apparently Tumblr replaces actual plus signs (%2B) with spaces (November 2014)
		var normalizedtag = decodeURIComponent(val.toLowerCase()).replace(/[\+\-]/, ' ');
		//Check tag page
		if((locref.length >= 3) && (locref[1] == 'tagged') && (decodeURIComponent(locref[2]).replace(/[\+\-]/, ' ') == normalizedtag)) {
			isspecial = true;
		}
		//Check post with tag
		if(locref[1] == 'post') {
			//Try a few different combinatons
			var $taglinks = $('.tags').find('a');
			$taglinks = $taglinks.add('a.tag');
			$taglinks = $taglinks.add($('.tag').find('a'));
			$taglinks.each(function() {
				var tagmatch = $(this).attr('href').match(new RegExp('//' + options.domain + '/tagged/([^/]*)'));
				if(tagmatch && (decodeURIComponent(tagmatch[1].toLowerCase()).replace(/[\+\-]/, ' ') == normalizedtag)) {
					isspecial = true;
				}
			});
		}
	});
	//Check pages
	$.each(options.pages, function(i, val) {
		if((locref.length >= 2) && (decodeURIComponent(locref[1].toLowerCase()) == val.toLowerCase())) {
			isspecial = true;
		}
	});
	options.todo(isspecial);

	});
}